#include "bst.h"

// ---------------------------------------
// Node class
// Default constructor
Node::Node() {
// TODO: Implement this
	this->key = 0;
	this->parent = NULL;
	this->left = NULL;
	this->right = NULL;
}
// Constructor
Node::Node(int in) {
// TODO: Implement this
	this->key = in;
	this->parent = NULL;
	this->left = NULL;
	this->right = NULL;
}
// Destructor
Node::~Node() {
// TODO: Implement this
	delete this->left;
	delete this->right;
}

// Add parent 
void Node::add_parent(Node* in) {
// TODO: Implement this
	this->parent = in;
}
// Add to left of current node
void Node::add_left(Node* in) {
// TODO: Implement this
	this->left = in;
}
// Add to right of current node
void Node::add_right(Node* in) {
// TODO: Implement this
	this->right = in;
}

// Get key
int Node::get_key()
{
// TODO: Implement this
	return this->key;
}
// Get parent node
Node* Node::get_parent()
{
// TODO: Implement this
	return this->parent;
}
// Get left node
Node* Node::get_left()
{
// TODO: Implement this
	return this->left;
}
// Get right node
Node* Node::get_right()
{
// TODO: Implement this
	return this->right;
}
// Print the key to ostream to
// Do not change this
void Node::print_info(ostream& to)
{
    to << key << endl;
}
// ---------------------------------------


// ---------------------------------------
// BST class
// Walk the subtree from the given node
void BST::inorder_walk(Node* in, ostream& to)
{
// TODO: Implement this
	if (in != NULL)
	{
		inorder_walk(in->get_left(), to);
		in->print_info(to);
		inorder_walk(in->get_right(), to);
	}
}
// Constructor
BST::BST()
{
// TODO: Implement this
	this->root = NULL;
}
// Destructor
BST::~BST()
{
// TODO: Implement this
	delete this->root;
}
// Insert a node to the subtree
void BST::insert_node(Node* in)
{
// TODO: Implement this
	int side = 0;
	Node* currentNode = root;
	Node* previousNode = NULL;
	while (currentNode != NULL)
	{
		previousNode = currentNode;
		if (currentNode->get_key() > in->get_key())
		{
			side = 1;
			currentNode = currentNode->get_left();
		}
		else
		{
			side = 2;
			currentNode = currentNode->get_right();
		}
	}
	in->add_parent(previousNode);
	if (side == 0)
	{
		this->root = in;
	}
	else if (side == 1)
	{
		previousNode->add_left(in);
	}
	else
	{
		previousNode->add_right(in);
	}
}
// Delete a node to the subtree
void BST::delete_node(Node* out)
{
// TODO: Implement this
	Node* temp;
	Node* successor;
	if (out->get_left() != NULL && out->get_right() != NULL)
	{
		successor = get_succ(out);
		if (successor->get_parent() == out)
		{
			successor->add_parent(out->get_parent());
		}
		else
		{
			temp = successor->get_parent();
			temp->add_left(successor->get_right());
			if (temp->get_left() != NULL)
			{
				temp->get_left()->add_parent(temp);
			}
			successor->add_right(out->get_right());
		}
		if (out == this->root)
		{
			this->root = successor;
		}
		else
		{
			temp = out->get_parent();
			if (temp->get_left() == out)
			{
				temp->add_left(successor);
			}
			else
			{
				temp->add_right(successor);
			}
		}
		successor->add_parent(out->get_parent());
		successor->add_left(out->get_left());
		out->get_left()->add_parent(successor);
		if (out->get_right() != successor)
		{
			out->get_right()->add_parent(successor);
		}
	}
	else if (out->get_right() == NULL)
	{
		successor = out->get_left();
		if (out == this->root)
		{
			this->root = successor;
		}
		else
		{
			temp = out->get_parent();
			if (temp->get_left() == out)
			{
				temp->add_left(successor);
			}
			else
			{
				temp->add_right(successor);
			}
		}
		if (successor != NULL)
		{
			successor->add_parent(out->get_parent());
		}
	}
	else if (out->get_left() == NULL)
	{
		//cout << "r" << endl;
		//cout << "none " << out->get_right()->get_key() << endl;
		successor = out->get_right();
		if (out == this->root)
		{
			this->root = successor;
		}
		else
		{
			temp = out->get_parent();
			if (temp->get_left() == out)
			{
				temp->add_left(successor);
			}
			else
			{
				temp->add_right(successor);
			}
		}
		successor->add_parent(out->get_parent());
	}
	out->add_parent(NULL);
	out->add_left(NULL);
	out->add_right(NULL);
	delete out;
}
// minimum key in the BST
Node* BST::tree_min()
{
// TODO: Implement this
	return get_min(this->root);
}
// maximum key in the BST
Node* BST::tree_max()
{
// TODO: Implement this
	return get_max(this->root);
}
// Get the minimum node from the subtree of given node
Node* BST::get_min(Node* in)
{
// TODO: Implement this
	Node* currentNode = in;
	while (in != NULL && currentNode->get_left() != NULL)
	{
		currentNode = currentNode->get_left();
	}
	return currentNode;
}
// Get the maximum node from the subtree of given node
Node* BST::get_max(Node* in)
{
// TODO: Implement this
	Node* currentNode = in;
	while (in != NULL && currentNode->get_right() != NULL)
	{
		currentNode = currentNode->get_right();
	}
	return currentNode;
}
// Get successor of the given node
Node* BST::get_succ(Node* in)
{
// TODO: Implement this
	if (in->get_right() != NULL)
	{
		return get_min(in->get_right());
	}
	else
	{
		Node* node = in;
		Node* retNode = in->get_parent();
		while (retNode && node==retNode->get_right())
		{
			node = retNode;
			retNode = retNode->get_parent();
		}
		return retNode;
	}
}
// Get predecessor of the given node
Node* BST::get_pred(Node* in)
{
// TODO: Implement this
	if (in->get_left() != NULL)
	{
		return get_max(in->get_left());
	}
	else
	{
		Node* retNode = in->get_parent();
		Node* node = in;
		while (retNode && node == retNode->get_left())
		{
			node = retNode;
			retNode = retNode->get_parent();
		}
		return retNode;
	}
}
// Walk the BST from min to max
void BST::walk(ostream& to)
{
// TODO: Implement this
	inorder_walk(this->root, to);
}
// Search the tree for a given key
Node* BST::tree_search(int search_key)
{
// TODO: Implement this
	Node* currentNode = root;
	while (currentNode != NULL && currentNode->get_key() != search_key)
	{
		if (currentNode->get_key() > search_key)
		{
			currentNode = currentNode->get_left();
		}
		else
		{
			currentNode = currentNode->get_right();
		}
	}
	if (currentNode != NULL)
	{
		return currentNode;
	}
	else
	{
		return NULL;
	}
}
// ---------------------------------------

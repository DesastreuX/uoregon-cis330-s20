#include "numlist.h"

// Partitioning functions
// Serial partition
unsigned int NumList::partition(vector<int>& keys, unsigned int low, 
                                unsigned int high)
{
    // Use the last element as the pivot
    int pivot = keys[high];

    // TODO: Implement the serial partitioning method
    int i = (low - 1);
    int temp;
    for (unsigned int j = low; j <= high - 1; j++)
    {
        if (keys[j] < pivot)
        {
            i++;
            temp = keys[i];
            keys[i] = keys[j];
            keys[j] = temp;
        }
    }
    temp = keys[i + 1];
    keys[i + 1] = keys[high];
    keys[high] = temp;
    for (int i = 0; i < 10; i++)
    {
        cout << keys[i] << " ";
    }
    cout << endl;
    return (i + 1);
}

// Parallel partition
unsigned int NumList:: partition_par(vector<int>& keys, unsigned int low,
                                     unsigned int high)
{
    // Use the last element as the pivot
    int pivot = keys[high];


    // TODO: Implement the parallel partitioning method
    // There should be two #pragmas to parallelize the loop
    // First loop is calculating the lt and gt arrays
    // Second is when the integers are copied to the correct position (i.e.,
    // left or right side of the pivot
    int size = keys.size();
    vector<int> cpyK(keys.begin(), keys.end());
    vector<int> lt(size, 0);
    vector<int> gt(size, 0);

    #pragma omp parallel for
    for (unsigned int i = low; i < high; i++)
    {
        if (i == low && cpyK[i] < pivot)
        {
            lt[i] = 1;
        }
        else if (i == low)
        {
            gt[i] = 1;
        }
        else if (i != low && cpyK[i] < pivot)
        {
            lt[i] = 1;
        }
        else if (i != low)
        {
            gt[i] = 1;
        }
    }

    int countlt = 0;
    int countgt = 0;

    for (unsigned int i = low; i < high; i++)
    {
        if(lt[i] == 1)
        {
            countlt++;
        }
        lt[i] = countlt;
        if(gt[i] == 1)
        {
            countgt++;
        }
        gt[i] = countgt;
    }

    int pi = lt[(high - 1)];

    #pragma omp parallel for
    for (unsigned int i = low; i < high; i++)
    {
        if (lt[i] > 0 && i == low)
        {
            keys[pi - lt[i] + low] = cpyK[i];
        }
        else if (gt[i] > 0 && i == low)
        {
            keys[pi + gt[i] + low] = cpyK[i];
        }
        else if (lt[i] > lt[i - 1])
        {
            keys[pi - lt[i] + low] = cpyK[i];
        }
        else if (gt[i] > gt[i - 1])
        {
            keys[pi + gt[i] + low] = cpyK[i];
        }
    }
    keys[pi + low] = pivot;
    cpyK.clear();
    lt.clear();
    gt.clear();

    return pi + low;
}

// Actual qsort that recursively calls itself with particular partitioning
// strategy to sort the list
void NumList::qsort(vector<int>& keys, int low, int high, ImplType opt)
{
    if(low < high) {
        unsigned int pi;
        if(opt == serial) {
            pi = partition(keys, low, high);
        } else {
            pi = partition_par(keys, low, high);
        }
        qsort(keys, low, pi - 1, opt);
        qsort(keys, pi + 1, high, opt);
    }
}

// wrapper for calling qsort
void NumList::my_qsort(ImplType opt)
{
    /* Initiate the quick sort from this function */
    qsort(list, 0, list.size() - 1, opt);
}
// Default constructor
// This should "create" an empty list
NumList::NumList() {
    /* do nothing */
    /* you will have an empty vector */
}
// Contructor
// Pass in a vector and the partitioning strategy to use
NumList::NumList(vector<int> in, ImplType opt) {
    list = in;
    my_qsort(opt);
}
// Destructor
NumList::~NumList() {
    /* do nothing */
    /* vector will be cleaned up automatically by its destructor */
}
// Get the element at index
int NumList::get_elem(unsigned int index)
{
    return list[index];
}
// Print the list
void NumList::print(ostream& os)
{
    for(unsigned int i = 0; i < list.size(); i++) {
        os << i << ":\t" << list[i] << endl;
    }
}

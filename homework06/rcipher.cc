#include <string>
#include <iostream>
#include "rcipher.h"

// -------------------------------------------------------
// Caesar Cipher implementation
// -------------------------------------------------------
RCipher::RCipher()
{

}

RCipher::~RCipher()
{

}

string RCipher::encrypt(string raw)
{
	CCipher myCipher(13);
	string retStr = myCipher.encrypt(raw);
	return retStr;
}

string RCipher::decrypt(string enc)
{
	CCipher myCipher(13);
	string retStr = myCipher.decrypt(enc);
	return retStr;
}
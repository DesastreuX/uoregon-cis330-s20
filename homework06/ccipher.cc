#include <string>
#include <iostream>
#include <algorithm>
#include "ccipher.h"


// -------------------------------------------------------
// Caesar Cipher implementation


// -------------------------------------------------------
struct Cipher::CipherCheshire {
	string cipher_alpha;
};

CCipher::CCipher()
{
	this->smile->cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
}	

CCipher::CCipher(int offset)
{
	if (offset >= 0)
	{
		offset %= ALPHABET_SIZE;
		this->smile->cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
		rotate_string(this->smile->cipher_alpha, offset);
	}
	else
	{
		cerr << "Error: Caesar cipher is less than 0" << endl;
		exit(EXIT_FAILURE);
	}
}

CCipher::~CCipher()
{
	
}

string CCipher::encrypt(string raw)
{
	Cipher myCipher(this->smile->cipher_alpha);
	string retStr = myCipher.encrypt(raw);
	return retStr;
}

string CCipher::decrypt(string enc)
{
	Cipher myCipher(this->smile->cipher_alpha);
	string retStr = myCipher.decrypt(enc);
	return retStr;
}


// Rotates the input string in_str by rot positions
void rotate_string(string& in_str, int rot)
{
    // TODO: You will likely need this function. Implement it.
    string temp = in_str.substr(0, rot);
    in_str.erase(0, rot);
    in_str.append(temp);
}

#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"
#include "vcipher.h"


// -------------------------------------------------------
// Running Key Cipher implementation
// -------------------------------------------------------
struct Cipher::CipherCheshire {
	string keyword;
};

VCipher::VCipher()
{

}

VCipher::VCipher(string keyword)
{
	bool isValid = true;
	for (unsigned long i = 0; i < keyword.size(); i++)
	{
		if (keyword[i] == ' ')
		{
			isValid = false;
		}
	}
	if (keyword.size() < MAX_LENGTH && isValid)
	{
		this->smile->keyword = keyword;
	}
	else
	{
		cerr << "Error: not a valid Vigenere key word" << endl;
	}
}

VCipher::~VCipher()
{

}

string VCipher::encrypt(string raw)
{
	KCipher myCipher(this->smile->keyword);
	return myCipher.encrypt(raw);
}

string VCipher::decrypt(string enc)
{
	KCipher myCipher(this->smile->keyword);
	return myCipher.decrypt(enc);
}
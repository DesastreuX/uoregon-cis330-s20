#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"




/* Helper function definitions
 */
bool isspace(string str)
{
	bool ret = true;
	for (unsigned long i = 0; i < str.size(); i++)
	{
		if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z'))
		{
			ret = false;
		}
	}
	return ret;
}
bool isValidKey(string key)
{
	if (key.empty())
	{
		cerr << "Invalid Running key: " << endl;
		exit(EXIT_FAILURE);
	}
	else if (isspace(key))
	{
		cerr << "Invalid Running key: " << endl;
		exit(EXIT_FAILURE);
	}
	else if (key.size() > MAX_LENGTH)
	{
		cerr << "Invalid Running key: " << key << endl;
		exit(EXIT_FAILURE);
	}
	return true;
}

// -------------------------------------------------------
// Running Key Cipher implementation
// -------------------------------------------------------
struct Cipher::CipherCheshire {
	vector<string> book;
	unsigned int id;
};

KCipher::KCipher()
{
	this->smile->id = 0;
	string temp;
	for(unsigned int i = 0; i < MAX_LENGTH; i++)
	{
		temp += 'a';	
	}
	this->smile->book.push_back(temp);
}

KCipher::KCipher(string page)
{
	if (isValidKey(page))
	{
		this->smile->id = 0;
		string temp;
		for (unsigned long i = 0; i < page.size(); i++)
		{
			if (isupper(page[i]))
			{
				temp += LOWER_CASE(page[i]);
			}
			else if (!isspace(page[i]))
			{
				temp += page[i];
			}
		}
		this->smile->book.push_back(temp);
	}
}

KCipher::~KCipher()
{

}

void KCipher::add_key(string page)
{
	if (isValidKey(page))
	{
		string temp;
		for (unsigned long i = 0; i < page.size(); i++)
		{
			if (isupper(page[i]))
			{
				temp += LOWER_CASE(page[i]);
			}
			else if (!isspace(page[i]))
			{
				temp += page[i];
			}
		}
		this->smile->book.push_back(temp);
	}
}

void KCipher::set_id(unsigned int id)
{
	if (id < this->smile->book.size())
	{
		this->smile->id = id;
	}
	else
	{
		cerr << "Warning: invalid id: " << id << endl;
	}
}

string KCipher::encrypt(string raw)
{
	string retStr;
	int count = 0;
	char enc;
	string temp = this->smile->book[this->smile->id];
	unsigned long size = temp.size();
	char temp_c[temp.size() + 1];
	temp.copy(temp_c, temp.size() + 1);
	for (unsigned long i = 0; i < raw.size(); i++)
	{
		if (raw[i] == ' ')
		{
			retStr += raw[i];
		}
		else
		{
			enc = LOWER_CASE(raw[i]) + temp_c[count % size];
			if (isupper(raw[i]))
			{
				if (enc > 'z')
				{
					enc -= ALPHABET_SIZE;
				}
				retStr += UPPER_CASE(enc);
			}
			else
			{
				retStr += LOWER_CASE(enc);
			}	
			count++;
		}
	}
	return retStr;
}

string KCipher::decrypt(string enc)
{
	string retStr;
	int count = 0;
	char dec;
	string temp = this->smile->book[this->smile->id];
	unsigned long size = temp.size();
	char temp_c[temp.size() + 1];
	temp.copy(temp_c, temp.size() + 1);
	for (unsigned long i = 0; i < enc.size(); i++)
	{
		if (enc[i] == ' ')
		{
			retStr += enc[i];
		}
		else
		{
			dec = LOWER_CASE(enc[i]) - temp_c[count % size];
			if (isupper(enc[i]))
			{
				if (dec < 'a')
				{
					dec += ALPHABET_SIZE;
				}
				retStr += UPPER_CASE(dec);
			}
			else
			{
				retStr += LOWER_CASE(dec);
			}
			count++;
		}
	}
	return retStr;
}